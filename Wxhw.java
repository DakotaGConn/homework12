import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import com.google.gson.*;

public class Wxhw
{

  String wxReport = null;

   
   public String getURL(String zipCode)
	{
		JsonElement jse = null;


		try
		{
			// Construct WxStation API URL
			URL bitlyURL = new URL("http://api.wunderground.com/api/716973fa0f3aaf1a/geolookup/conditions/q/" + zipCode + ".json");

			// Open the URL
			InputStream is = bitlyURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
      // Build a weather report
      	
      try
      {
         String error = jse.getAsJsonObject().get("response").getAsJsonObject().get("error").getAsJsonObject().get("description").getAsString();
         wxReport = "ERROR: " + error;
      }
         
      catch (java.lang.NullPointerException npe)
      {
      String city = jse.getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("display_location")
      .getAsJsonObject().get("full").getAsString();
      wxReport = "Location: " + city + "\n";

      String time = jse.getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("observation_time").getAsString();
      wxReport = wxReport + "Time: " + time + "\n";
      
      String weather = jse.getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("weather").getAsString();
      wxReport = wxReport + "Weather: " + weather + "\n";

      String temp = jse.getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("temp_f").getAsString();
      wxReport = wxReport + "Temperature F: " + temp + "\n";
      
      String wind = jse.getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("wind_string").getAsString();
      wxReport = wxReport + "Wind: " + time + "\n";

      String pressure = jse.getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("pressure_in").getAsString();
      wxReport = wxReport + "Pressure in HG: " + pressure + "\n";
   }
	
         }
    return wxReport;
	}

	public static void main(String[] args)
	{
	   String CLzipCode;
      String result;
      
      Wxhw b = new Wxhw();
    if ( args.length == 0 )
    {
      System.out.println("Please enter a zip code.");
      System.exit(1);
    }
    
		CLzipCode = b.getURL(args[0]);
      if ( CLzipCode != null )
      {
		    System.out.println(CLzipCode);
      }
    
	}
}
